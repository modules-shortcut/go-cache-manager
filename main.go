package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/modules-shortcut/go-cache-manager/cache"
	"gitlab.com/rumahlogic/go-cheat-sheet/utils"
	"time"
)

type Name struct {
	ID   int
	Name string
}

func main() {
	app := gin.Default()
	c := cache.Redis{}

	utils.ErrorHandler(c.Init())

	app.GET("/cache/:key", func(ctx *gin.Context) {
		var (
			names []Name
			err   error
		)

		if err = c.Get(ctx.Param("key"), &names); err != nil {
			utils.ErrorHandler(err)
			ctx.JSON(400, err.Error())
			return
		}

		utils.PrettyPrint(names)
		ctx.JSON(200, names)

	})

	app.POST("/cache/:key", func(ctx *gin.Context) {
		var (
			names []Name
			name  Name
			err   error
		)

		names = append(names, Name{ID: 1, Name: "dayat"}, Name{ID: 2, Name: "otong"})

		if err = ctx.ShouldBindJSON(&name); err != nil {
			utils.ErrorHandler(err)
			ctx.JSON(400, err.Error())
			return
		}
		if err = c.Set(ctx.Param("key"), names, 1*time.Minute); err != nil {
			utils.ErrorHandler(err)
			ctx.JSON(400, err.Error())
			return
		}

		utils.PrettyPrint(names)
		ctx.JSON(200, names)
	})

	app.Run(":8080")
}
