package cache

import (
	"errors"
	"github.com/patrickmn/go-cache"
	"time"
)

const defaultExpiration = 24 * time.Hour

// InMemory is an implementation of the cache interface which stores values
// in-memory.
type InMemory struct {
	cache      *cache.Cache
	expiration time.Duration
}

// NewInMemory creates a new InMemory instance with default values.
func NewInMemory() *InMemory {
	return &InMemory{
		cache:      cache.New(-1, defaultExpiration),
		expiration: defaultExpiration,
	}
}

// WithExpiration updates the expiration value of `c`.
func (c *InMemory) WithExpiration(d time.Duration) *InMemory {
	c.expiration = d
	return c
}

// Get retrieves a value from the InMemory cache implementation.
func (c *InMemory) Get(key string) (interface{}, error) {
	v, found := c.cache.Get(key)
	if !found {
		return nil, errors.New("inMemory: nil")
	}
	return v, nil
}

// Set sets a value for a key in the InMemory cache implementation.
func (c *InMemory) Set(key string, value interface{}) error {
	c.cache.Set(key, value, c.expiration)
	return nil
}

// Del delete data.
func (c *InMemory) Del(key string) error {
	c.cache.Delete(key)
	return nil
}

// Items get all data.
func (c *InMemory) Items() map[string]cache.Item {
	return c.cache.Items()
}

// Count total data.
func (c *InMemory) Count() int {
	return c.cache.ItemCount()
}

// Flush clear all data in cache.
func (c *InMemory) Flush() {
	c.cache.Flush()
	return
}
