package cache

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/caarlos0/env/v6"
	"github.com/go-redis/redis/v8"
	"gitlab.com/modules-shortcut/go-cache-manager/utils"
	"time"
)

type Config struct {
	REDIS_ON   bool   `env:"REDIS_ON" envDefault:"false" json:"redis_on"`
	REDIS_HOST string `env:"REDIS_HOST" envDefault:"localhost" json:"redis_host"`
	REDIS_PORT string `env:"REDIS_PORT" envDefault:"6379" json:"redis_port"`
	REDIS_PASS string `env:"REDIS_PASS" envDefault:"" json:"redis_pass"`
}

func (this *Config) Parse() (err error) {
	return env.Parse(this)
}

type Redis struct {
	config Config
	cache  *redis.Client
	ctx    context.Context
}

func (r *Redis) Init() (err error) {
	if err = r.config.Parse(); err != nil {
		utils.ErrorHandler(err)
		return
	}

	r.cache = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", r.config.REDIS_HOST, r.config.REDIS_PORT),
		Password: r.config.REDIS_PASS,
	})
	r.ctx = context.Background()
	return
}

func (r *Redis) Client() *redis.Client {
	return r.cache
}

func (r *Redis) Get(key string, data interface{}) (err error) {
	var res []byte

	if !r.config.REDIS_ON {
		return errors.New("redis: nil")
	}

	if res, err = r.cache.Get(r.ctx, key).Bytes(); err != nil {
		return
	}
	utils.Convert(res, data)
	return
}

func (r *Redis) Set(key string, data_ interface{}, ttl time.Duration) (err error) {
	var val []byte
	if val, err = json.Marshal(data_); err != nil {
		utils.ErrorHandler(err)
		return
	}
	return r.cache.Set(r.ctx, key, string(val), ttl).Err()
}

func (r *Redis) Del(key string) (err error) {
	return r.cache.Del(r.ctx, key).Err()
}

func (r *Redis) Flush() (err error) {
	return r.cache.FlushDB(r.ctx).Err()
}
