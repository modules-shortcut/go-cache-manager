build:
	echo "BUILD START"
	docker build -t registry.gitlab.com/modules-shortcut/go-cache-manager:dev .
	echo "BUILD DONE"

push:
	echo "PUSH START"
	docker push registry.gitlab.com/modules-shortcut/go-cache-manager:dev
	echo "PUSH DONE"

reload:
	echo "EXEC SHELL SCRIPT ON SERVER"
	ssh devops@192.168.51.13 CACHE.sh

aio:
	make build && make push && make reload

dev:
	DB_HOST=192.168.51.51 DB_PORT=9106 DB_NAME=feeds DB_USER=user DB_PASS=password DB_SSL=disable TIME_ZONE=Asia/Jakarta go run main.go -p 8000
