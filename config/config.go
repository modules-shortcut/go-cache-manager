package config

import "gitlab.com/modules-shortcut/go-cache-manager/utils"

var (
	REDIS_HOST = utils.GetEnvString("REDIS_HOST", "localhost")
	REDIS_PORT = utils.GetEnvString("REDIS_PORT", "6379")
	REDIS_USER = utils.GetEnvString("REDIS_USER", "user")
	REDIS_PASS = utils.GetEnvString("REDIS_PASS", "password")
)
