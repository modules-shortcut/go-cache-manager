package utils

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"time"
)

// PrettyPrint for print struct/map/array to console as indent view
func PrettyPrint(data interface{}) {
	if data != nil {
		p, err := json.MarshalIndent(data, "", "\t")
		if err != nil {
			ErrorHandler(err)
			return
		}
		fmt.Printf("[%s]\n%s \n", GetEnvString("ENGINE_NAME", "UNDEFINED"), p)
	}

}

// ErrorHandler for print error message to console with engine name if error != nil
func ErrorHandler(err error) {
	if err != nil {
		var (
			_, file, line, _ = runtime.Caller(1)
			loc, _           = time.LoadLocation("Asia/Jakarta")
			timeStr          = time.Now().In(loc).Format("2006/01/02 15:04:05")
		)

		fmt.Printf("[%s] %s\n%s %s:%d\n", GetEnvString("ENGINE_NAME", "UNDEFINED"), err.Error(), timeStr, file, line)
	}
}

// LogPrint for print message for debug with engine name
func LogPrint(args ...interface{}) {
	if true {
		fmt.Printf("[%s] ", GetEnvString("ENGINE_NAME", "UNDEFINED"))
		_, _ = fmt.Fprintln(os.Stdout, args...)
	}
}

// GetEnvString for get env type string
func GetEnvString(key, defaultVal string) string {
	value, exist := os.LookupEnv(key)
	if !exist {
		return defaultVal
	}
	return value
}

// Convert for convert from map[string]interface{} to struct or reverse or array of them
func Convert(source interface{}, destination interface{}) {
	var (
		jsonBody []byte
		err      error
	)

	if val, ok := source.(string); ok {
		jsonBody = []byte(val)
	} else if val1, ok1 := source.([]byte); ok1 {
		jsonBody = val1
	} else {
		if jsonBody, err = json.Marshal(source); err != nil {
			ErrorHandler(err)
		}
	}

	ErrorHandler(json.Unmarshal(jsonBody, destination))
	return
}
